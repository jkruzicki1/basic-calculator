// Jacob Kruzicki
// Assignment 2

#include <iostream>
#include <conio.h>

using namespace std;

float Add(float num1, float num2)
{
    return num1 + num2;
}

float Subtract(float num1, float num2)
{
    return num1 - num2;
}

float Multiply(float num1, float num2)
{
    return num1 * num2;
}

bool Divide(float num1, float num2, float& answer)
{
    if (num2 == 0)
    {
        return false;
    }
    else
    {
        answer = num1 / num2;
        return true;
    }
}

float Power(float base, int exp)
{
    float n = 1;

    if (exp > 0)
    {
        for (int i = 0; i < exp; ++i)
        {
            n *= base;
        }
    }
    else if (exp < 0)
    {
        for (int i = 0; i > exp; --i)
        {
            n /= base;
        }
    }

    return n;
}

int main()
{
    float num1, num2, ans = 0;
    char op = '\0';

    do
    {
        cout << "Enter number 1: ";
        cin >> num1;

        cout << "Enter number 2: ";
        cin >> num2;

        cout << "Enter the operator to use: ";
        cin >> op;

        switch (op)
        {
        case '+':
            ans = Add(num1, num2);
            break;
        case '-':
            ans = Subtract(num1, num2);
            break;
        case '*':
            ans = Multiply(num1, num2);
            break;
        case '/':
            if (!Divide(num1, num2, ans))
            {
                cout << "Error: Cannot divide by zero" << endl;
            }
            break;
        case '^':
            ans = Power(num1, num2);
            break;
        default:
            cout << "Error: Invalid operator" << endl;
            ans = NAN;
            break;
        }


        cout << "Answer: " << ans << endl;

        cout << "Press 'q' to quit. Press any other key to do another operation." << endl;
        op = _getch();
    } while (op != 'q' && op != 'Q');

    return 0;
}
